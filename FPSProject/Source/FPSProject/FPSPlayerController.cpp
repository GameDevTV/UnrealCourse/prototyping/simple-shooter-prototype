// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSPlayerController.h"
#include "Blueprint/UserWidget.h"

void AFPSPlayerController::BeginPlay()
{
    Super::BeginPlay();

    HUD = CreateWidget<UUserWidget>(this, HUDClass);
    if (HUD != nullptr)
    {
        HUD->AddToViewport();
    }
}

void AFPSPlayerController::GameHasEnded(class AActor *EndGameFocus, bool bIsWinner) 
{
    Super::GameHasEnded(EndGameFocus, bIsWinner);

    UUserWidget* EndScreen;
    if (!bIsWinner)
    {
        EndScreen = CreateWidget<UUserWidget>(this, LoseScreenClass);
        if (EndScreen != nullptr)
        {
            EndScreen->AddToViewport();
        }
    }
    else
    {
        EndScreen = CreateWidget<UUserWidget>(this, WinScreenClass);
        if (EndScreen != nullptr)
        {
            EndScreen->AddToViewport();
        }
    }

    HUD->RemoveFromViewport();

    if (EndScreen != nullptr)
    {
        FInputModeUIOnly InputMode;
        InputMode.SetWidgetToFocus(EndScreen->GetRootWidget()->TakeWidget());
        SetInputMode(InputMode);
        bShowMouseCursor = true;
    }
}

void AFPSPlayerController::PlayAgain() 
{
    FInputModeGameOnly InputMode;
    SetInputMode(InputMode);
    bShowMouseCursor = false;

    RestartLevel();
}