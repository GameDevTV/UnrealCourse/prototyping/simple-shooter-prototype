// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "FPSAIController.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API AFPSAIController : public AAIController
{
	GENERATED_BODY()

public:
	AFPSAIController();
	virtual void Tick(float DeltaSeconds);

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere)
	class UBehaviorTree* AIBehavior;
 };
