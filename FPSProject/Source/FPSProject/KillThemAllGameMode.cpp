// Fill out your copyright notice in the Description page of Project Settings.


#include "KillThemAllGameMode.h"
#include "EngineUtils.h"
#include "AIController.h"
#include "FPSCharacter.h"


void AKillThemAllGameMode::PawnKilled(AController *KilledController) 
{
    Super::PawnKilled(KilledController);

    APlayerController* PlayerController = Cast<APlayerController>(KilledController);
    if (PlayerController != nullptr)
    {
        EndGame(false);
        return;
    }

    for (AAIController* EnemyAI : TActorRange<AAIController>(GetWorld()))
    {
        AFPSCharacter* EnemyCharacter = Cast<AFPSCharacter>(EnemyAI->GetPawn());
        if (EnemyCharacter != nullptr && !EnemyCharacter->IsDead())
        {
            return;
        }
    }
    EndGame(true);
}

void AKillThemAllGameMode::EndGame(bool bIsPlayerWinner) 
{
    for (AController *Controller : TActorRange<AController>(GetWorld()))
    {
        bool bShouldWin = Controller->IsPlayerController() ? bIsPlayerWinner : !bIsPlayerWinner;
        Controller->GameHasEnded(Controller->GetPawn(), bIsPlayerWinner);
    }
}
