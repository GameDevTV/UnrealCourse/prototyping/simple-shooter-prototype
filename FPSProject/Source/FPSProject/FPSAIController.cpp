// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"

AFPSAIController::AFPSAIController()
{
    // Should call super here? Or done by default?
}

void AFPSAIController::BeginPlay() 
{
    Super::BeginPlay();
    if (AIBehavior != nullptr)
    {
        RunBehaviorTree(AIBehavior);

        GetBlackboardComponent()->SetValueAsVector(TEXT("StartLocation"), GetPawn()->GetActorLocation());
    }
}

void AFPSAIController::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}
