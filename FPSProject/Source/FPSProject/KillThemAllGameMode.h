// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FPSProjectGameModeBase.h"
#include "KillThemAllGameMode.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API AKillThemAllGameMode : public AFPSProjectGameModeBase
{
	GENERATED_BODY()

public:
	virtual void PawnKilled(AController *KilledController) override;

private:
	void EndGame(bool bIsPlayerWinner);
	
};
